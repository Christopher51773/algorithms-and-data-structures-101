#!/usr/bin/env python3
# -*- coding: utf-8 -*

from activations import relu, softmax
import numpy as np


class Input:
	"""docstring for ClassName"""

	def __init__(self, input):
		self.input = input

	def getInput(self):
		return self.input


class Convolutional2D:
	"""docstring for ClassName"""

	def __init__(self, filters=None, filterChannels=None, inputShape=None, kernelSize=(3, 3), stride=1, activation='relu'):
		self.noFilters = filters
		self.stride = stride
		self.filterChannels = filterChannels
		self.kernelSize = kernelSize
		self.inputShape = inputShape
		self.activation = activation
		self.filters = []  # needs to be an array of filters
		self.initialised = False

	def initialise(self, featureMap):
		if(self.inputShape is None):
			self.inputShape = featureMap.shape
		if(self.filterChannels is None):
			self.filterChannels = featureMap.shape[2]
		#self.filters = [np.random.randn(self.kernelSize[0], self.kernelSize[1], self.inputShape[2]) for i in range(self.noFilters)]
		self.filters = [np.full((self.kernelSize[0], self.kernelSize[
								1], self.inputShape[2]), 2) for i in range(self.noFilters)]
		for filt in self.filters:
			filt.fill(2.0)

		self.initialised = True

	def inputRegionsGenerator(self, featureMap):
		rows, columns, depth = featureMap.shape
		filterw, filterh = self.kernelSize

		for row in range(rows - (filterw - self.stride)):
			for column in range(columns - (filterh - self.stride)):
				region = featureMap[
					row:(row + filterw), column:(column + filterh)]
				yield row, column, region

	def convolve(self, filt, featureMap):
		rows, cols, depth = featureMap.shape
		filterw, filterh = self.kernelSize
		reductionw = filterw - self.stride
		reductionh = filterh - self.stride
		result = np.zeros((rows - reductionw, cols - reductionh))
		for row, col, region in self.inputRegionsGenerator(featureMap):
			regionResult = region * filt
			result[row, col] = np.sum(regionResult)

		return(result)

	def forwardPass(self, featureMap):
		# if first call generate filters
		if(not self.initialised):
			self.initialise(featureMap)
		self.previousMap = featureMap
		row, col, deptth = featureMap.shape
		filterw, filterh = self.kernelSize
		reductionw = filterw - self.stride
		reductionh = filterh - self.stride
		# assert filter depth equals featureMap input channels
		# assert equal filter dimensions
		# assert odd filter dimensions - because we are trying to do an average neighbourhood encoding of pixel values
		# generate output feature map template array
		output = np.zeros((row - reductionw, col - reductionh, self.noFilters))
		# convolve image with filters
		# for eachfilter call convolve
		# for each filter and featureMap convolve and combine all channels
		# stack returned convolutions

		for i in range(len(self.filters)):
			filt = self.filters[i]
			result = self.convolve(filt, featureMap)
			output[:, :, i] = result

		return relu(output)

	def backProp(self, gradients, learningRate):
		pass


class MaxPool2D:
	"""docstring for ClassName"""

	def __init__(self, poolSize=(2, 2), step=2):
		self.poolSize = poolSize
		self.step = step

	# check h and w are the right way around
	def inputRegionsGenerator(self, featureMap):
		rows, columns, depth = featureMap.shape
		poolw, poolh = self.poolSize
		nw = rows // poolw
		nh = columns // poolh

		for i in range(nh):
			for j in range(nw):
				region = featureMap[
					(i * poolw):(i * poolw + self.step), (j * poolh):(j * poolh + self.step)]
				yield region, i, j

	def forwardPass(self, featureMap):
		rows, columns, depth = featureMap.shape
		self.previousMap = featureMap
		output = np.zeros(
			(rows // self.poolSize[0], columns // self.poolSize[1], depth))
		for region, i, j in self.inputRegionsGenerator(featureMap):
			output[i, j] = np.amax(region, axis=(0, 1))
		return output

	def backProp(self, gradients, learningRate):
		output = np.zeros(self.previousMap)
		poolw, poolh = self.poolSize
		for region, i, j in self.inputRegionsGenerator(self.previousMap):
			rows, cols, deepth = region.shape
			amax = np.amax(region, axis=(0, 1))
			for row in range(row):
				for col in range(сols):
					for dep in range(depth):
						if region[row,col,dep] == amax[dep]:
							output[i*poolw + row, j * poolh + col, dep] = gradients[i, j, dep]

		return output

		pass


class Dense:
	"""docstring for ClassName"""

	def __init__(self, labels=None, activation='softmax'):
		self.labels = labels
		self.activation = activation
		self.outputNodes = len(labels)
		self.noInputFeatures = None
		self.weights = []
		self.biases = []
		self.initialised = False

	def initialise(self, featureMap):
		self.noInputFeatures = len(featureMap)
		self.weights = np.random.randn(
			self.noInputFeatures, self.outputNodes) / self.noInputFeatures
		# print(self.weights)
		self.biases = np.zeros(self.outputNodes)
		self.initialised = True

	def forwardPass(self, featureMap):
		self.previousShape = featureMap.shape
		output = featureMap.flatten()
		if(not self.initialised):
			self.initialise(output)
		self.previousMap = output
		self.previousTotals = np.dot(output, self.weights) + self.biases
		return softmax(output, self.weights, self.biases)

	def backProp(self, gradients, learningRate):
		for i, gradient in enumerate(gradients):
			if gradient == 0:
				continue

			totalsExp = np.exp(self.previousTotals - np.max(self.previousTotals))
			sumTotExpo = np.sum(totalsExp)
			diffOutTot = -totalsExp[i] * totalsExp/(sumTotExpo**2)
			diffOutTot[i] = totalsExp[i] * (sumTotExpo - totalsExp[i]) / (sumTotExpo**2)

			#Gradients of totals with respect to weights/biases/input
			diffGradWeights = self.previousMap
			diffGradBiases = 1
			diffGradInputs = self.weights

			#Gradient loss with respect to totals
			diffLoss = gradient * diffOutTot

			#Gradients of totals with respect to weights/biases/input
			diffLossWeights = diffGradWeights[np.newaxis].T @ diffLoss[np.newaxis]
			diffLossBiases = diffLoss * diffGradBiases
			diffLossInputs = diffGradInputs @ diffLoss

			#Update weights
			self.weights -= learningRate * diffLossWeights
			self.biases -=learningRate * diffLossBiases
			return diffLossInputs.reshape(self.previousShape)
