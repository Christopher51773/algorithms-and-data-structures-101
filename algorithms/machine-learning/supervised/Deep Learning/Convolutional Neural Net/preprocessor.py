#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Third Party Imports
from os import makedirs
from os.path import split, basename
from PIL import Image, ImageOps
import sys
import glob
from docopt import docopt
from math import floor, ceil
import random
import shutil
import numpy as np


class DataProcessor:

	def __init__(self, path, trainingSets, trainin_valid_test_ratio, imageSize):
		self.images = []
		self.labels = []
		self.testSet = []
		self.validSet = []
		self.predictSet = []

		# Create training set data directories
		for directory in DATA_DIRECTORIES:
			for dataset in args['--trainingSets']:
				category = basename(dataset)
				labels.append(category)
				trainingDirectories.append(
					'./data/' + directory + '/' + category)
			try:
				makedirs(trainingDirectories[-1])
			except OSError as error:
				print(error)

		# Remove duplicate labels
		labels = list(set(labels))

		# Assign training data directory
		traingDataPath = split(trainingDirectories[0])[0]

		# Partition training data into data directories
		for folder in args['--trainingSets']:
			noTrainingSamples = len(glob.glob(folder + '/*.jpg'))
			sampleSizes = (floor(TRAINING_VALID_TEST_RATIO[0] * noTrainingSamples), floor(TRAINING_VALID_TEST_RATIO[
						   1] * noTrainingSamples), ceil(TRAINING_VALID_TEST_RATIO[2] * noTrainingSamples))
			i = 0
			print(folder)
			for ratio in TRAINING_VALID_TEST_RATIO:
				print('Moving ' + basename(folder) +
					  ' into ' + DATA_DIRECTORIES[i] + 'folder')
				for sample in random.sample(glob.glob(folder + '/*.jpg'), sampleSizes[i]):
					shutil.move(sample, './data/' +
								DATA_DIRECTORIES[i] + '/' + basename(folder))

				i += 1

		# Convert images to 28x28 size
		print("Transforming images to reqired dimensions")

		for directory in trainingDirectories:
			img_paths = glob.glob(directory + '/*.jpg')
			for path in img_paths:
				im_pth = path

				im = Image.open(im_pth)
				old_size = im.size  # old_size[0] is in (width, height) format

				ratio = float(IMAGE_SIZE) / max(old_size)
				new_size = tuple([int(x * ratio) for x in old_size])

				im.thumbnail(new_size, Image.ANTIALIAS)

				# create a new image and paste the resized on it
				new_im = Image.new("RGB", (IMAGE_SIZE, IMAGE_SIZE))
				new_im.paste(im, ((IMAGE_SIZE - new_size[0]) // 2,
								  (IMAGE_SIZE - new_size[1]) // 2))
				im.close()
				try:
					new_im.save(im_pth)
				except Exception as e:
					raise e
