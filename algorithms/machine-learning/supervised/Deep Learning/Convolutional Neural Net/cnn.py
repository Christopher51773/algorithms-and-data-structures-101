#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Author: Christopher Ellis
Version: 1.0

This is an implementation of a basic convolution neural net (CNN) for classifying images. The neural net consists of the following six layers (four of which are hidden):

L0: Input Layer
L1: Convolutional Layer with three filters (With Relu activation function)
L2: Pooling Layer (With Relu activation function)
L3: Convolutional Layer with five filters (With Relu activation function)
L4: Pooling Layer (With Relu activation function)
L5: Fully Connected Layer (with Softmax activation)


cellis@christopher-ellis.uk

Usage:
  cnn --trainingSets <path>... --epoch <epochs> [--channels=<channels>]
  cnn -h | --help
  cnn --version

Options:
  -h --help                                         Show this screen
  -v --version                                      Show version
  -t --trainingSets=<path>...                       Path to training datasets e.g. ./cats ./dogs
  --epoch=<epochs>                                  Number of training epochs
  --channels=<channels>                             Number of channels for each input images e.g. a standard RGB image would have 3 channels [default: 3]
"""


# Third Party Imports
from os import makedirs
from os.path import split, basename
from PIL import Image, ImageOps
import sys
import glob
from docopt import docopt
from math import floor, ceil
import random
import shutil
import numpy as np

# Project Imports
from model import Model
from layers import Convolutional2D, MaxPool2D, Dense
from preprocessor import DataProcessor


# Print usage documentation
if __name__ == "__main__":
	args = docopt(__doc__, version=1.0)
	print(__doc__)


# Constants
TRAINING_VALID_TEST_RATIO = (0.6, 0.1, 0.3)
DATA_DIRECTORIES = ['training',
					'validation', 'test']
BATCH_SIZE = 10
STEP = 1
PADDING = 'same'
IMAGE_SIZE = 100

# Global Variables
trainingDirectories = []
trainingData = []
validData = []
testData = []
traingDataPath = ''
labels = []
model = Model(BATCH_SIZE, STEP, PADDING)

# Utility functions
print(args)

# Prepare training data

# Create training set data directories
for directory in DATA_DIRECTORIES:
	for dataset in args['--trainingSets']:
		category = basename(dataset)
		labels.append(category)
		trainingDirectories.append('./data/' + directory + '/' + category)
		try:
			makedirs(trainingDirectories[-1])
		except OSError as error:
			print(error)

# Remove duplicate labels
labels = list(set(labels))

# Assign training data directory
traingDataPath = split(trainingDirectories[0])[0]

# Partition training data into data directories
for folder in args['--trainingSets']:
	noTrainingSamples = len(glob.glob(folder + '/*.jpg'))
	sampleSizes = (floor(TRAINING_VALID_TEST_RATIO[0] * noTrainingSamples), floor(TRAINING_VALID_TEST_RATIO[
				   1] * noTrainingSamples), ceil(TRAINING_VALID_TEST_RATIO[2] * noTrainingSamples))
	i = 0
	print(folder)
	for ratio in TRAINING_VALID_TEST_RATIO:
		print('Copying ' + basename(folder) +
			  ' into ' + DATA_DIRECTORIES[i] + 'folder')
		for sample in random.sample(glob.glob(folder + '/*.jpg'), sampleSizes[i]):
			shutil.copy(sample, './data/' +
						DATA_DIRECTORIES[i] + '/' + basename(folder))

		i += 1


# Convert images to 28x28 size
print("Transforming images to reqired dimensions")

for directory in trainingDirectories:
	img_paths = glob.glob(directory + '/*.jpg')
	for path in img_paths:
		im_pth = path
		print('Converting image ' + str(path))
		try:
			im = Image.open(im_pth)
			old_size = im.size  # old_size[0] is in (width, height) format

			ratio = float(IMAGE_SIZE) / max(old_size)
			new_size = tuple([int(x * ratio) for x in old_size])

			im.thumbnail(new_size, Image.ANTIALIAS)

			# create a new image and paste the resized on it
			new_im = Image.new("RGB", (IMAGE_SIZE, IMAGE_SIZE))
			new_im.paste(im, ((IMAGE_SIZE - new_size[0]) // 2,
							  (IMAGE_SIZE - new_size[1]) // 2))
			im.close()
			new_im.save(im_pth)			
		except Exception as e:
			raise e

# Generate training, valid and test datasets with labels
print("Generate labelled training, valid and test datasets")
for i, label in enumerate(labels):
	trainingData += [(path, i) for path in glob.glob('./data/' +
													 DATA_DIRECTORIES[0] + '/' + labels[i] + '/*.jpg')]
	validData += [(path, i) for path in glob.glob('./data/' +
													  DATA_DIRECTORIES[1] + '/' + labels[i] + '/*.jpg')]
	testData += [(path, i) for path in glob.glob('./data/' +
													 DATA_DIRECTORIES[2] + '/' + labels[i] + '/*.jpg')]

# Construct Model
model.input(trainingData, dim=int(args['--channels']))
model.addLayer(Convolutional2D(filters=3, kernelSize=(3, 3), filterChannels=int(
	args['--channels']), inputShape=(IMAGE_SIZE, IMAGE_SIZE, int(args['--channels'])), activation='relu'))  # 28x28x3 --> 26x26x3
model.addLayer(MaxPool2D(poolSize=(2, 2), step=2))        # 26x26x3 --> 13x13x3
model.addLayer(Convolutional2D(filters=5, kernelSize=(
	3, 3), activation='relu'))  # 13x13x3 --> 11x11x5
model.addLayer(MaxPool2D(poolSize=(2, 2), step=2))        # 11x11x5 --> 5x5x5
model.addLayer(Dense(labels=labels, activation='softmax'))  # 5x5x5 --> 125x2x1

# Show model summary
model.summary()

# Generate test images
testImages = []
for i in range(1000):
	label = random.randint(0, 1)
	channel = np.random.rand(28,28)
	im = np.full((28,28,3),0)
	im[:,:,0] = channel
	im[:,:,1] = channel
	im[:,:,2] = channel
	testImages.append((im, label))

# Traim CNN
print("\nTraining CNN...")
for i in range(int(args["--epoch"])):
 	print('\nEpoch ' + str(i))
 	model.train(testData, 0.005)

# Test CNN
#model.test()

# # Save trained model
# print("Saving trained model")
# model.save(args["--save-model-path"])

# # Use CNN
# # Load saved model
# print('Loading saved model...')
# model.load()

# print("Model loaded successfully")
# print("Predicting...")
# for i in range(args["--epoch"]):
#     model.run(args['predictionDataset'])
