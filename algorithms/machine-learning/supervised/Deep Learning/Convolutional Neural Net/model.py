#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Third Party Imports
import glob
import numpy as np
from PIL import Image
from pprint import pprint
import json
from random import *

# Project Imports
from layers import Input


class Model:

		"""docstring for Model"""

		def __init__(self, batchSize, step, padding, trainingData=None, learningRate=0.005, dim=None):
				self.batchSize = batchSize
				self.step = step
				self.padding = padding
				self.learningRate = learningRate
				self.layers = []
				self.result = None
				if(trainingData is not None and dim is not None):
						self.input(trainingData, dim)

		def addLayer(self, layer):
				self.layers.append(layer)
				print('Added ' + type(layer).__name__ + ' layer')
				if(type(layer).__name__ == 'Dense' and layer.__dict__['activation'] == 'softmax'):
						print('Model constructed')

		def input(self, trainingData, dim):
				self.trainingData = trainingData
				self.dimensions = dim
				w, h = Image.open(self.trainingData[0][0]).size
				print('\nBuilding model...')
				self.addLayer(Input(np.zeros([w, h, dim], dtype=float)))

		def test(self):
				print("\nTesting CNN...")
				loss = 0
				num_correct = 0

				testImages = []
				for i in range(1000):
					label = randint(0, 1)
					channel = np.random.rand(28,28)
					im = np.full((28,28,3),0)
					im[:,:,0] = channel
					im[:,:,1] = channel
					im[:,:,2] = channel
					testImages.append((im, label))

				for i, img in enumerate(testImages):
					#print('width: ' + str(im.shape[0]))
					#print('height: ' + str(im.shape[1]))
					#print('channels: ' + str(im.shape[2]))
					result, l, acc = self.forwardPass(img[0], img[1])

					# calculate initial gradient
					gradients = np.zeros(2)
					gradients[label] = -1 / result[label]

					# backProp
					#gradients = self.layers[-1].backProp(gradients, 0.005) #Softmax backProp

					loss += l
					num_correct += acc
					if i % 100 == 99:
						print('[Step %d] Past 100 steps: Average Loss %.3f | Accuracy: %d%%' %
							(i + 1, loss / 100, num_correct))
						loss = 0
						num_correct = 0

		def summary(self):
				print('\nModel summary')
				pprint(repr(self.layers))

		def load():
				pass

		def save():
				pass

		def forwardPass(self, image, label):
				for i in range(1, len(self.layers)):
						layer = self.layers[i]
						w, h, c = image.shape
						self.result = image
						#print('Layer ' + str(i) + ' input')
						# print(self.result.shape)
						# print(self.result)
						self.result = layer.forwardPass(featureMap=self.result)
						#print('Layer ' + str(i) + ' output')
						# print(self.result.shape)
						# print(self.result)
				loss = -np.log(self.result[label])
				acc = 1 if np.argmax(self.result) == label else 0
				return self.result, loss, acc

		def backProp(self, gradients, learningRate):
				for i in reversed(range(1, len(self.layers))):
						layer = self.layers[i]
						print(gradients)
						gradients = layer.backProp(gradients, learningRate)
				return gradients

		def train(self, trainingSet, learningRate=0.005):
				loss = 0
				num_correct = 0
				self.learningRate = learningRate
				noCategories = len(set([img[1] for img in trainingSet]))
				for i, img in enumerate(trainingSet):
					#print('Processing image ' + str(i))
					image = Image.open(img[0])
					im=np.asarray(image)
					image.close()
					label = img[1]
					# forward pass
					result, l, acc = self.forwardPass(im, label)
					#print(l, acc, result)

					# calculate initial gradient
					#gradients = np.zeros(noCategories)
					#gradients[label] = -1 / result[label]

					# backProp
					#gradients = self.backProp(gradients, learningRate)
					#gradients = self.layers[-1].backProp(gradients, 0.005) #Softmax backProp

					loss += l
					num_correct += acc
					if i % 100 == 99:
						#image = Image.fromarray(im, 'RGB')
						#image.show()
						print('[Step %d] Past 100 steps: Average Loss %.3f | Accuracy: %d%%' %
									(i + 1, loss / 100, num_correct))
						loss = 0
						num_correct = 0


		def predict(self, image):
				label = ''
				return image, label
