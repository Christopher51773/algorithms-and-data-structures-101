#!/usr/bin/env python3
# -*- coding: utf-8 -*
import numpy as np


def relu(featureMap):
	featureMap[featureMap < 0] = 0
	return featureMap


def softmax(featureMap, weights, biases):
	totals = np.dot(featureMap, weights) + biases
	exp = np.exp(totals - np.max(totals))
	return exp / np.sum(exp, axis=0)
